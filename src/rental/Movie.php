<?php
namespace rental;

class Movie
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var int
     */
    public $rentalType;

    /**
     * Movie constructor.
     * @param string $name
     * @param int $rentalType
     */
    public function __construct($name, $rentalType)
    {
        $this->name = $name;
        $this->rentalType = $rentalType;
    }
}

